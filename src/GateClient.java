/**
 * Created by info on 07.03.14.
 */

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.encoding.StandardEncoding;
import sun.misc.IOUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Milan Dojchinovski
 *         <milan (at) dojchinovski (dot) mk>
 *         Twitter: @m1ci
 *         www: http://dojchinovski.mk
 */
public class GateClient {

    // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;

    // whether the GATE is initialised
    private static boolean isGateInitilised = false;

    public String saveNewContentToFile(String newContent) {
        try {
            String fileName = "/Users/info/IdeaProjects/ddw-hw-1/resources/tmp-files/template.xml";
            File templateFile = new File(fileName);

            fileName = "/Users/info/IdeaProjects/ddw-hw-1/resources/tmp-files/file.xml";
            File file = new File(fileName);

            if (!file.exists()) {
                file.createNewFile();
            }

            try {
                String content = FileUtils.readFileToString(templateFile, "UTF-8");
                content = content.replaceAll("REPLACE_ME_COMMENT", newContent);
                FileUtils.writeStringToFile(file, content, "UTF-8");
            } catch (IOException e) {
                //Simple exception handling, replace with what's necessary for your use case!
                throw new RuntimeException("Generating file failed", e);
            }

            return fileName;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    public void train(Boolean isEval) {
        if (!isGateInitilised) {

            // initialise GATE
            initialiseGate();
        }

        try {
            // create an instance of a Document Reset processing resource
            FeatureMap resetFeatureMap = Factory.newFeatureMap();
            resetFeatureMap.put("setsToKeep", "Key");
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR", resetFeatureMap);

            FeatureMap annotationFMap = Factory.newFeatureMap();
            if (!isEval) {
                annotationFMap.put("annotationTypes", "comment");
                annotationFMap.put("inputASName", "Key");
                annotationFMap.put("copyAnnotations", true);
                annotationFMap.put("outputASName", "");
                annotationFMap.put("tagASName", "");
            }
            ProcessingResource annotationSetTransfer = (ProcessingResource) Factory.createResource("gate.creole.annotransfer.AnnotationSetTransfer", annotationFMap);

            ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");
            ProcessingResource sentenceSplitterPR = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");
            ProcessingResource taggerPR = (ProcessingResource) Factory.createResource("gate.creole.POSTagger");

            ProcessingResource morphologicalAnalyser = (ProcessingResource) Factory.createResource("gate.creole.morph.Morph");

            FeatureMap batchFeatureMap = Factory.newFeatureMap();
            File batchFile = new File("/Users/info/Dropbox/FIT/magistr/2-semestr/DDW/hw1/paum.xml");
            java.net.URI batchUri = batchFile.toURI();
            try {
                batchFeatureMap.put("configFileURL", batchUri.toURL());
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL of JAPE grammar");
                System.out.println(e.toString());
            }

            batchFeatureMap.put("inputASName", "");
            batchFeatureMap.put("outputASName", "");
            if (isEval) {
                batchFeatureMap.put("learningMode", "EVALUATION");
            } else {
                batchFeatureMap.put("learningMode", "TRAINING");
            }
            ProcessingResource batchLearningPR = (ProcessingResource) Factory.createResource("gate.learning.LearningAPIMain", batchFeatureMap);

            // create corpus pipeline
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            // add the processing resources (modules) to the pipeline
            annotationPipeline.add(documentResetPR);
            annotationPipeline.add(annotationSetTransfer);
            annotationPipeline.add(tokenizerPR);
            annotationPipeline.add(sentenceSplitterPR);
            annotationPipeline.add(taggerPR);
            annotationPipeline.add(morphologicalAnalyser);
            annotationPipeline.add(batchLearningPR);

            // create a corpus and populate
            Corpus corpus = Factory.newCorpus("");

            FileFilter ff = new FileFilter() {
                @Override
                public boolean accept(File pathname) {
//                    System.out.println(pathname);
                    return true;
                }
            };

            try {
                File directory;
                if (isEval) {
                    directory = new File("/Users/info/IdeaProjects/ddw-hw-1/resources/corpora/all");
                } else {
                    directory = new File("/Users/info/IdeaProjects/ddw-hw-1/resources/corpora/training");
                }
                java.net.URI corpusUri = directory.toURI();
                corpus.populate(corpusUri.toURL(), ff, "utf-8", false);
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL");
                System.out.println(e.toString());
            } catch (IOException e) {
                System.out.println("IO Exception in corpus populate");
                System.out.println(e.toString());
            }
            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);

            //run the pipeline
            annotationPipeline.execute();

        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void run(Boolean runCorpus, String newContent) {

        if (!isGateInitilised) {

            // initialise GATE
            initialiseGate();
        }

        try {
            // create an instance of a Document Reset processing resource
            FeatureMap resetFeatureMap = Factory.newFeatureMap();
//            resetFeatureMap.put("setsToKeep", "Key");
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR", resetFeatureMap);

            FeatureMap annotationFMap = Factory.newFeatureMap();
//            annotationFMap.put("annotationTypes", "comment");
//            annotationFMap.put("copyAnnotations", true);
//            annotationFMap.put("inputASName", "Key");
//            annotationFMap.put("outputASName", "");
//            annotationFMap.put("tagASName", "");
            ProcessingResource annotationSetTransfer = (ProcessingResource) Factory.createResource("gate.creole.annotransfer.AnnotationSetTransfer", annotationFMap);

            // locate the JAPE grammar file
            File japeOrigFile = new File("/Users/info/IdeaProjects/ddw-hw-1/resources/copy_comment_spans.jape");
            java.net.URI japeURI = japeOrigFile.toURI();

            // create feature map for the transducer
            FeatureMap transducerFeatureMap = Factory.newFeatureMap();
            try {
                // set the grammar location
                transducerFeatureMap.put("grammarURL", japeURI.toURL());
                transducerFeatureMap.put("inputASName", "Key");
                transducerFeatureMap.put("outputASName", "");
                // set the grammar encoding
                transducerFeatureMap.put("encoding", "UTF-8");
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL of JAPE grammar");
                System.out.println(e.toString());
            }

            // create an instance of a JAPE Transducer processing resource
            ProcessingResource japeTransducerPR = (ProcessingResource) Factory.createResource("gate.creole.Transducer", transducerFeatureMap);


            ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");
            ProcessingResource sentenceSplitterPR = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");
            ProcessingResource taggerPR = (ProcessingResource) Factory.createResource("gate.creole.POSTagger");

            ProcessingResource morphologicalAnalyser = (ProcessingResource) Factory.createResource("gate.creole.morph.Morph");

            FeatureMap batchFeatureMap = Factory.newFeatureMap();
            File batchFile = new File("/Users/info//IdeaProjects/ddw-hw-1/resources/paum.xml");
            java.net.URI batchUri = batchFile.toURI();
            try {
                batchFeatureMap.put("configFileURL", batchUri.toURL());
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL");
                System.out.println(e.toString());
            }

            batchFeatureMap.put("inputASName", "");
            batchFeatureMap.put("learningMode", "APPLICATION");
            batchFeatureMap.put("outputASName", "Output");
            ProcessingResource batchLearningPR = (ProcessingResource) Factory.createResource("gate.learning.LearningAPIMain", batchFeatureMap);


            // create corpus pipeline
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            // add the processing resources (modules) to the pipeline
            annotationPipeline.add(documentResetPR);
            annotationPipeline.add(annotationSetTransfer);
            annotationPipeline.add(japeTransducerPR);
            annotationPipeline.add(tokenizerPR);
            annotationPipeline.add(sentenceSplitterPR);
            annotationPipeline.add(taggerPR);
            annotationPipeline.add(morphologicalAnalyser);
            annotationPipeline.add(batchLearningPR);

            // create a corpus and add the document
            Corpus corpus = Factory.newCorpus("");

            // create a document
            if (!runCorpus) {
                try {
                    File origFile = new File(this.saveNewContentToFile(newContent));
                    java.net.URI fileURI = origFile.toURI();
                    Document document = Factory.newDocument(fileURI.toURL());
                    corpus.add(document);
                } catch (IOException e) {
                    System.out.println("IOException");
                    System.out.println(e.toString());
                }
            } else {

                FileFilter ff = new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
//                        System.out.println(pathname);
                        return true;
                    }
                };

                try {
                    File directory = new File("/Users/info/IdeaProjects/ddw-hw-1/resources/corpora/testing");
                    java.net.URI corpusUri = directory.toURI();
                    corpus.populate(corpusUri.toURL(), ff, "utf-8", false);
                } catch (MalformedURLException e) {
                    System.out.println("Malformed URL");
                    System.out.println(e.toString());
                } catch (IOException e) {
                    System.out.println("IO Exception in corpus populate");
                    System.out.println(e.toString());
                }
            }

            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);

            //run the pipeline
            annotationPipeline.execute();

            // loop through the documents in the corpus
            for (int i = 0; i < corpus.size(); i++) {

                Document doc = corpus.get(i);

                // get the default annotation set
                AnnotationSet as_default = doc.getAnnotations("Output");

                // get all Token annotations
                FeatureMap fmap = Factory.newFeatureMap();
                AnnotationSet annSetTokens = as_default.get("comment", fmap);

                ArrayList tokenAnnotations = new ArrayList(annSetTokens);

                // looop through the Token annotations
                for (int j = 0; j < tokenAnnotations.size(); ++j) {

                    // get a token annotation
                    Annotation token = (Annotation) tokenAnnotations.get(j);

                    // get the underlying string for the Token
                    Node isaStart = token.getStartNode();
                    Node isaEnd = token.getEndNode();
                    String underlyingString = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();

                    // get the features of the token
                    FeatureMap annFM = token.getFeatures();

                    // get the value of the "string" feature
                    Float prob = (Float) annFM.get((Object) "prob");
                    String rating = (String) annFM.get((Object) "rating");
                    if (rating != "5_Star_Review") {
                        System.out.println("Token: " + underlyingString);
                        System.out.println("Prob: " + prob + "\tRating: " + rating);
                        System.out.println();
                    }
                }
            }
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initialiseGate() {

        try {
            // set GATE home folder
            // Eg. /Applications/GATE_Developer_7.1
            File gateHomeFile = new File("/Applications/GATE_Developer_7.1");
            Gate.setGateHome(gateHomeFile);

            // set GATE plugins folder
            // Eg. /Applications/GATE_Developer_7.1/plugins
            File pluginsHome = new File("/Applications/GATE_Developer_7.1/plugins");
            Gate.setPluginsHome(pluginsHome);

            // set user config file (optional)
            // Eg. /Applications/GATE_Developer_7.1/user.xml
            Gate.setUserConfigFile(new File("/Applications/GATE_Developer_7.1", "user.xml"));

            // initialise the GATE library
            Gate.init();

            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            URL toolsHome = new File(pluginsHome, "Tools").toURL();
            URL learningHome = new File(pluginsHome, "Learning").toURL();

            register.registerDirectories(annieHome);
            register.registerDirectories(toolsHome);
            register.registerDirectories(learningHome);


            // flag that GATE was successfuly initialised
            isGateInitilised = true;

        } catch (MalformedURLException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
