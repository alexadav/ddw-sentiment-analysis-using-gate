/**
 * Created by info on 07.03.14.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Milan Dojchinovski
 * <milan (at) dojchinovski (dot) mk>
 * Twitter: @m1ci
 * www: http://dojchinovski.mk
 */
public class GATEEmbedded {

    public static void main(String[] args) {

        GateClient client = new GateClient();

        client.train(false);
        client.run(true, "");
//        client.train(true);
        while (true) {
            try {
                System.out.println("Write your review to process:");
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                String newContent = bufferRead.readLine();
                client.run(false, newContent);
            } catch (IOException e) {
                System.out.println("IO Exception");
                System.out.println(e.toString());
            }
        }
    }
}

